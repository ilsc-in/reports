package com.secure.reports.service;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Enumeration;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;

/**
 * This class contains general utility methods required for the application.
 *
 * @author Veerababu Barre
 */
public class Utility {

    private static ResourceBundle propsAgile;
    private static Logger logger = Logger.getLogger(Utility.class);
    private static Utility utilInstance = null;

    /**
     * This method is used to get the instance of the Utility class An object of
     * Utility class is created only when it is not available.(SingleTon
     * Pattern)
     *
     * @return Instance of Utility class
     */
    public static Utility getInstance() {
        if (utilInstance == null) {
            utilInstance = new Utility();
            utilInstance.init("DB_CONNECTION_AGILE");
        }
        return utilInstance;

    }

    /**
     * This method is used to load the properties specified in the properties
     * file
     *
     * @param appPropertyFileName String Application specific properties file name
     */
    public void init(String appPropertyFileName) {
        File file = new File(System.getProperty("secure.config", "/app/soft/agile936/agile/integration/sdk/extensions"));
        URL[] urls = new URL[1];
        try {
            urls[0] = file.toURI().toURL();
        } catch (Exception e) {
            logger.error("Failed to set base path while loading properties file " + appPropertyFileName + ".", e);
        }
        ClassLoader loader = new URLClassLoader(urls);
        try {
            propsAgile = ResourceBundle.getBundle(appPropertyFileName, Locale.getDefault(), loader);
        } catch (Exception e) {
            logger.error("Failed to load properties file " + appPropertyFileName + ".", e);
        }

        Properties props = new Properties();

        for (Enumeration e = propsAgile.getKeys(); e.hasMoreElements(); ) {
            String key = e.nextElement().toString();
            props.setProperty(key, propsAgile.getString(key));
        }

        PropertyConfigurator.configure(props);
        props.clear();
    }

    /**
     * This method is used to retrieve the Application specific properties.
     *
     * @param propertyName String Key in the properties file
     * @return String Value of the specified key
     */
    public String getAgileProperty(String propertyName) {
        String propValue = null;
        try {
            if (propertyName != null) {
                propValue = propsAgile.getString(propertyName);
            }
        } catch (Exception ex) {
            logger.error("Failed to read property " + propertyName + ".", ex);
        }
        return propValue;
    }

    /**
     * This method is used to retrieve the Application specific Agile
     * properties.
     *
     * @param propertyName String Key inthe properties file
     * @return String Value of the specified key
     */
    public String getProperty(String propertyName) {
        String propValue = null;
        if (propertyName != null) {
            propValue = propsAgile.getString(propertyName);
        }
        return propValue;
    }

    /**
     * This method is used to get Locale
     *
     * @return Locale
     */
    public Locale getLocale() {
        return Locale.getDefault();
    }

    /**
     * Returns the Stack Trace of an Exception as a String.
     *
     * @param t -
     *          Throwable Object whose Stack Trace is Required
     * @return Stack Trace of the specified Throwable Object as String
     */
    public String exception2String(Throwable t) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = null;
        try {
            pw = new PrintWriter(sw, true);
            t.printStackTrace(pw);
            return sw.toString();
        } finally {
            if (pw != null) {
                pw.flush();
                pw.close();
            }
        }
    }
}
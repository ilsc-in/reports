package com.secure.reports.service;

import com.secure.reports.model.PropertyNames;
import com.secure.reports.model.UserDetails;
import oracle.jdbc.pool.OracleDataSource;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.Random;


public class DBOperation {

    public Connection fxcon = null;
    static Statement stmt = null;
    private static Logger logger = Logger.getLogger(DBOperation.class);

    public Connection getConnection(String driverName, String url,
                                    String userName, String password) throws Exception {
        Connection con = null;
        Class.forName(driverName);
        try {
            OracleDataSource ods = new OracleDataSource();
            ods.setURL(url);
            ods.setUser(userName);
            ods.setPassword(password);

            con = ods.getConnection();
        } catch (SQLException sqlException) {
            for (int i = 0; i < 5; i++) {
                if (con == null) {
                    con = DriverManager.getConnection(url, userName, password);
                }
            }
        }
        return con;
    }

    public ArrayList<UserDetails> GetUserDataFromDB() {
        String driver = PropertyNames.AGILE_DATABASE_DRIVER;
        String hostName = PropertyNames.AGILE_DATABASE_HOSTNAME;
        String portNo = PropertyNames.AGILE_DATABASE_PORT_NO;
        String user = PropertyNames.AGILE_DATABASE_USER_NAME;
        String password = PropertyNames.AGILE_DATABASE_PASSWORD;
        String sid = PropertyNames.AGILE_DATABASE_SID;
        String url = PropertyNames.AGILE_START_DB_URL + hostName + ":" + portNo + "/" + sid;

        String un, ld, fn, nd;
        ArrayList<UserDetails> UserDataList = new ArrayList<>();
        try {

            fxcon = getConnection(driver, url, user, password);
            stmt = fxcon.createStatement();
            String Query = "SELECT   au.loginid,nvl( TO_CHAR(MAX (login_time),'DD-MON-YYYY'),'NOT LOGIN YET') last_login_date , au.first_name || ' ' || au.last_name NAME,"
                    + " ROUND ((MONTHS_BETWEEN (SYSDATE,MAX (login_time)) * 30), 0) num_of_days"
                    + " FROM agile.agileuser au left join agile.user_usage_history uuh "
                    + " on  au.loginid = uuh.username where au.enabled = 1 "
                    + " GROUP BY au.loginid, au.first_name, last_name"
                    + " ORDER BY au.first_name";
            ResultSet rs;
            for (rs = stmt.executeQuery(Query); rs.next(); ) {

                un = rs.getString("loginid");
                ld = rs.getString("last_login_date");
                fn = rs.getString("NAME");
                nd = rs.getString("num_of_days");

                UserDataList.add(new UserDetails(un, ld, fn, nd));
            }
        } catch (Exception e) {
            logger.error("Failed to get data from database.", e);
        } finally {
            try {
                fxcon.close();
            } catch (SQLException e) {
                logger.error("Error closing database connection.", e);
            }
        }
        return UserDataList;
    }

    public static ArrayList<UserDetails> getSampleData() {
        ArrayList<UserDetails> list = new ArrayList<>();
        Random ran = new Random();
        for (int i = 0; i < 10; i++) {
            String days = "" + ran.nextInt(6) + 5;
            list.add(new UserDetails("u" + i, i+" Jan, 2019", "user " + i, days));
        }
        return list;
    }
}

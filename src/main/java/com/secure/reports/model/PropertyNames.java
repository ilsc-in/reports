package com.secure.reports.model;

import com.secure.reports.service.Utility;
import org.apache.log4j.Logger;

import java.lang.reflect.Field;

/**
 * This class acts as a constants file.
 *
 * @author Veerababu Barre
 */


public class PropertyNames {

    /**
     * public declaration
     */
    public static String AGILE_DOMAIN;
    /**
     * public declaration
     */
    public static String AGILE_USERNAME;
    /**
     * public declaration
     */
    public static String AGILE_PASSWORD;
    /**
     * public declaration
     */
    public static String QUERY_ECO;
    /**
     * public declaration
     */
    public static String BASE_ID_SUBSTITUTE_PART;
    /**
     * public declaration
     */
    public static String BASE_ID_MUTUAL_EXCLUSIVE;
    /**
     * public declaration
     */
    public static String BASE_ID_MUTUAL_OPTIONAL;
    /**
     * public declaration
     */
    public static String BASE_ID_REFERENCE_DESIGNATOR;
    /**
     * public declaration
     */
    public static String BASE_ID_IS_ECO_PROCESSED;
    /**
     * public declaration
     */
    public static String BASE_ID_PARTS_TEMPLATE;
    /**
     * public declaration
     */
    public static String BASE_ID_PARTS_UOM;
    /**
     * public declaration
     */
    public static String BASE_ID_ITEM_SAFETY_CRITICAL;
    /**
     * public declaration
     */
    public static String BASE_ID_ITEM_ROHS;
    public static String BASE_ID_ITEM_REACH;

    /**
     * public declaration
     */
    /**
     * public declaration
     */
    public static String BASE_ID_BOM_CONFIG_NOTES;
    /**
     * public declaration
     */
    public static String BASE_ID_BOM_REFERENCE_ITEM_NUMBER;
    /**
     * public declaration
     */
    public static String BASE_ID_ITEM_UPDATED_IN_EBIZ;
    /**
     * public declaration
     */
    public static String BASE_ID_DOCUMENT_UPDATED_IN_EBIZ;
    /**
     * public declaration
     */
    public static String BASE_ID_BOM_NUMBER;
    /**
     * public declaration
     */
    public static String BASE_ID_MATERIAL_NUMBER;
    /**
     * public declaration
     */
    public static String BASE_ID_MATERIAL_DESCRIPTION;
    /**
     * public declaration
     */
    public static String BASE_ID_WORK_INST;
    /**
     * public declaration
     */
    public static String BASE_ID_REQUESTER;
    /**
     * public declaration
     */
    public static String ITEM_PRODUCTION_LIFECYCLE_PHASE;
    /**
     * public declaration
     */
    public static String ITEM_CREATED_AGILE_STATUS_SUCCESS;
    /**
     * public declaration
     */
    public static String ITEM_CREATED_AGILE_STATUS_ERROR;
    /**
     * public declaration
     */
    public static String ECO_UNPROCESSED_WORKFLOWS;
    /**
     * public declaration
     */
    public static String ECO_IS_ECO_PROCESSED_SUCCESS;
    /**
     * public declaration
     */
    public static String ECO_IS_ECO_PROCESSED_ERROR;
    /**
     * public declaration
     */

    public static String ECO_IS_ECO_PROCESSED_INPROGRESS;
    /**
     * public declaration
     */

    public static String ECO_STATUS_RELEASED;
    /**
     * public declaration
     */

    public static String ORG_IMO;
    /**
     * public declaration
     */
    public static String ORG_S1I;
    /**
     * public declaration
     */

    public static String DATE_FORMAT;
    /**
     * public declaration
     */
    public static String SUB_CLASSES;
    /**
     * public declaration
     */
    public static String SUB_CLASSES_WORK_INST;
    /**
     * public declaration
     */

    public static String EMAIL_SERVER;
    /**
     * public declaration
     */
    public static String EMAIL_FROM_ID;
    public static String BASE_ID_HORSTMANN_REFERENCE;
    public static String BASE_ID_CEWE_REFERENCE;
    public static String BASE_ID_SECURE_REFERENCE;
    /**
     * public declaration
     */
    public static String EXCEPTION_TO_EMAIL_ID;
    /**
     * public declaration
     */
    public static String EXCEPTION_SUBJECT;
    /**
     * public declaration
     */
    public static String EXCEPTION_MESSAGE;
    /**
     * public declaration
     */
    public static String EMAIL_MESSAGE_FAILED_ITEMS_HEDADER;
    /**
     * public declaration
     */
    public static String EMAIL_MESSAGE_FAILED_ITEMS_FOOTER;
    /**
     * public declaration
     */
    public static String EMAIL_NOTIFICATION_SUBJECT;
    /**
     * public declaration
     */
    public static String EMAIL_NOTIFICATION_STATUS_FOOTER_ONE;
    /**
     * public declaration
     */
    public static String EMAIL_NOTIFICATION_STATUS_FOOTER_TWO;
    /**
     * Public declaration
     */
    public static String NOTIFIED_EMAIL_ID;
    /**
     * public declaration
     */
    public static String DATABASE_DRIVER;
    /**
     * public declaration
     */
    public static String DATABASE_HOSTNAME;
    /**
     * public declaration
     */
    public static String DATABASE_PORT_NO;
    /**
     * public declaration
     */
    public static String DATABASE_USER_NAME;
    /**
     * public declaration
     */
    public static String DATABASE_PASSWORD;
    /**
     * public declaration
     */
    public static String DATABASE_SID;
    /**
     * public declaration
     */
    public static String DATABASE_PROGRAM_STATUS_COMPLETED;
    /**
     * public declaration
     */
    public static String DATABASE_PROGRAM_STATUS_EXECUTING;
    public static String AGILE_DATABASE_DRIVER;
    public static String AGILE_DATABASE_HOSTNAME;
    public static String AGILE_DATABASE_PORT_NO;
    public static String AGILE_DATABASE_SID;
    public static String AGILE_DATABASE_USER_NAME;
    public static String AGILE_DATABASE_PASSWORD;
    public static String AGILE_START_DB_URL;

    private static Utility util = Utility.getInstance();
    private static Logger logger = Logger.getLogger(PropertyNames.class);

    static {
        for (Field field : PropertyNames.class.getFields()) {
            try {
                field.set(null, util.getAgileProperty(field.getName()));
            } catch (Exception ex) {

            }
        }
    }
}

package com.secure.reports.controllers;

import com.secure.reports.model.UserDetails;
import com.secure.reports.service.DBOperation;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

@Controller
@SessionAttributes("UserFinList")
public class MainController {

    @RequestMapping("/viewReport")
    public ModelAndView viewReport(ModelMap model) {
        ArrayList<UserDetails> list =  new DBOperation().GetUserDataFromDB();
//        ArrayList<UserDetails> list = DBOperation.getSampleData();
        model.put("UserFinList", list);
        ModelAndView mv = new ModelAndView("loginReport", model);
        return mv;
    }

    @RequestMapping("/getReport")
    public void getReport(@SessionAttribute ArrayList<UserDetails> UserFinList,
                          HttpServletResponse response) throws IOException {
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "inline; filename=employee.xls");
        String content = "\"Login Id\"\t\"Name\"\t\"Last Login Date\"\t\"Number of Days\"\r\n";
        for (UserDetails user : UserFinList) {
            content += "\"" + user.getLoginID() + "\"\t\""
                    + user.getName() + "\"\t\""
                    + user.getLast_login_date() + "\"\t\""
                    + user.getNum_of_days() + "\"\r\n";
        }
        OutputStream os = response.getOutputStream();
        os.write(content.getBytes());
        os.flush();
    }
}

package com.secure.reports.model;

public class UserDetails {
	String LoginID,Last_login_date,Name,num_of_days;

	public UserDetails(String loginID, String last_login_date, String name,
			String num_of_days) {
		super();
		this.LoginID = loginID;
		this.Last_login_date = last_login_date;
		this.Name = name;
		this.num_of_days = num_of_days;
	}

	public String getLoginID() {
		return this.LoginID;
	}

	public void setLoginID(String loginID) {
		this.LoginID = loginID;
	}

	public String getLast_login_date() {
		return this.Last_login_date;
	}

	public void setLast_login_date(String last_login_date) {
		this.Last_login_date = last_login_date;
	}

	public String getName() {
		return this.Name;
	}

	public void setName(String name) {
		this.Name = name;
	}

	public String getNum_of_days() {
		return this.num_of_days;
	}

	public void setNum_of_days(String num_of_days) {
		this.num_of_days = num_of_days;
	}

}
